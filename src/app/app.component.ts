import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { BoredService } from './services/bored.service';
import { ActivityModel, MainButtonModel } from './models';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppComponent implements OnInit {

  initData: string;
  mainButton: MainButtonModel;
  activity: ActivityModel;
  buttonText: string = 'Yes, I am';

  constructor(
    private boredService: BoredService,
    private cdr: ChangeDetectorRef,
  ) {
  }

  ngOnInit(): void {
    this.mainButton = window.Telegram?.WebApp?.MainButton;
    this.initData = window.Telegram?.WebApp?.initData;
    this.mainButton.setText('Yes, I am');
    this.mainButton.onClick(() => this.loadRandomActivity());
    this.mainButton.show();
  }

  loadRandomActivity() {
    this.mainButton.showProgress();
    this.boredService.getRandomActivity()
      .subscribe({
        next: (activity: ActivityModel) => {
          this.activity = activity;
          this.mainButton.setText('I\'m still bored');
          this.buttonText = 'I\'m still bored';
          this.mainButton.hideProgress();
          this.cdr.detectChanges();
        },
        error: () => {
            this.mainButton.hideProgress();
            this.cdr.detectChanges();
          }
      })
  }
}
