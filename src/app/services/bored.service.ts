import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivityModel } from '../models';

@Injectable({
  providedIn: 'root'
})
export class BoredService {

  constructor(private http: HttpClient) { }

  getRandomActivity() {
    return this.http.get<ActivityModel>('https://www.boredapi.com/api/activity/');
  }
}
