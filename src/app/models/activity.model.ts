export interface ActivityModel {
  activity: string;
  type: 'education' | 'recreational' | 'social' | 'diy' | 'charity' | 'cooking' | 'relaxation' | 'music' | 'busywork';
  participants: number;
  price: number;
  link: string;
  key: string;
  accessibility: number;
}
