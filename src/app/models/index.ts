import { MainButtonModel } from './main-button.model';

export * from './activity.model';
export * from './main-button.model';

declare global {
  interface Window {
    Telegram: {
      WebApp: {
        MainButton: MainButtonModel;
        initData: string;
      }
    };
  }
}
