export interface MainButtonModel {
  setText: (text: string) => MainButtonModel;
  onClick: (f: Function) => MainButtonModel;
  show: () => MainButtonModel;
  showProgress: () => MainButtonModel;
  hideProgress: () => MainButtonModel;
}
